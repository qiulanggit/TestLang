# coding:utf-8

# # 冒泡排序
# list = [8,4,7,3,1,9,2]    #待排序列表
# list1 = []               # 排序后列表
# for i in range(len(list)):  #获取待排序列表次数
#     M = max(list)          # 获取列表最大值排序效果是降序，修改max为min就是获取最小值排序效果就是升序
#     list1.append(M)        # 将最大值 添加到 list1列表
#     list.remove(M)         # 删除list最大值---然后再循获取最大值添加到list1--再删除
# print(list1)




def bubble_sort(items):
    for i in range(len(items) - 1):             # 6
        flag = False
        for j in range(len(items) - 1 - i): # J = 2
            if items[j] > items[j + 1]:  #2  3
                items[j], items[j + 1] = items[j + 1], items[j]
                print(items)
                flag = True
        if not flag:
            break
    return items

print(bubble_sort([8,4,7,3,1,9,2]))




