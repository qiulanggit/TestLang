# coding:utf-8

from zlj_api.requestsapi import RunMain
import json


''' APP接口集合 '''

class AppAapi(RunMain):
    # 登录接口
    def login_index(self):
        url = 'https://user.zhaoliangji.com/api/login/index'
        method = 'POST'
        headers = {'Connection': 'keep-alive'}
        params = {'username': '15629561737',
                  'password': 'ql123456'}
        response = RunMain.run_main(url, method, params, headers)
        response_json = json.loads(response)        # 返回参数str转换为json
        response = response_json['data']['token']
        return response

    def coupon_list(self):
        url = 'https://user.zhaoliangji.com/api/coupon/list'
        method = 'GET'
        headers = {'Connection': 'keep-alive'}
        params = {'user_id': '23353818',
                  'status': '1',
                  'token': ''
                  }


if __name__=='__main__':
    getapi = AppAapi()
    response = getapi.login_index()
    print(response)

