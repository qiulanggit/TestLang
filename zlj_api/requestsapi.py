# coding:utf-8

import requests
import json

''' 接口请求封装 '''


class RunMain(object):
    @classmethod
    def send_get(self, url, data, headers=None):
        res = requests.get(url=url, data=data, headers=headers)
        res = res.json()
        return res

    @classmethod
    def send_post(self, url, data, headers=None):
        res = requests.post(url=url, data=data, headers=headers)  # 返回一个对象
        res = res.json()  # 使用requst第三方库json将对象转换为字典
        return res

    @classmethod
    def run_main(self, url, method, params, headers=None):
        if method == 'POST':
            res = self.send_post(url, params, headers)
        else:
            res = self.send_get(url, params, headers)
        return res


if __name__ == '__main__':
    url = 'http://user.zhaoliangji.com/api/login/index'
    params = {'username': '15629561737',
              "password": 'ql123456',
              'x_sensors_device_id': 'f84e326f0bb18b84'
              }
    method = 'POST'
    headers = {'Connection': 'keep-alive',
               'Host': 'user.zhaoliangji.com',
               'Accept-Encoding': 'gzip',
               'User-Agent': 'android/8.0.0 zhaoliangji/8.2.80',
               'X-App-Name': 'zlj',
               'X-App-Version': '8.2.80',
               'X-Device-Id': '43b51679-1663-37e3-a77a-798545443369',
               'X-System-Version': '8.0.0',
               'X-System-Type': 'android'}
    test = RunMain()
    respons = test.run_main(url, method, params, headers)
    print(respons)
