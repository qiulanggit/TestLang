# coding:utf-8
import os

# PROJECT_PATH = 上上级目录
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__) + os.path.sep + "..")


class StartFile(object):
    @classmethod
    def run_ddt(cls):
        os.system(r'python ' + PROJECT_PATH + r'\api_auto\run_ddt.py')


if __name__ == '__main__':
    start = StartFile()
    start.run_ddt()
