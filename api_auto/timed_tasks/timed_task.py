# coding:utf-8
import time
from api_auto.Start import StartFile

''' 定时任务 '''


class Task(object):
    @classmethod
    def timer_start(cls, times):
        while True:
            print('执行中...', time.strftime('%Y-%m-%d %X', time.localtime()))
            StartFile.run_ddt()  # 此处为要执行的任务
            print('执行结束', time.strftime('%Y-%m-%d %X', time.localtime()))
            time.sleep(times)



if __name__ == "__main__":
    Task.timer_start(10)
