# coding:utf-8
import flask, json
from flask import request, send_file, make_response, render_template, redirect, url_for, send_from_directory
from api_auto.Start import StartFile
from os import path
from api_auto.timed_tasks.timed_task import Task
import time
import os

'''
flask： web框架，通过flask提供的装饰器@server.route()将普通函数转换为服务
登录接口，需要传url、username、passwd
'''

# 创建一个服务，把当前这个python文件当做一个服务
server = flask.Flask(__name__)

# PROJECT_PATH = 上上级目录
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__) + os.path.sep + "..")


# server.config['JSON_AS_ASCII'] = False
# @server.route()可以将普通函数转变为服务 登录接口的路径、请求方式
@server.route('/login', methods=['get', 'post'])
def login():
    # 获取通过url请求传参的数据
    username = request.values.get('name')
    # 获取url请求传的密码，明文
    pwd = request.values.get('pwd')
    # 判断用户名、密码都不为空，如果不传用户名、密码则username和pwd为None
    if username and pwd:
        if username == 'xiaoming' and pwd == '111':
            result = {'code': 200, 'message': '登录成功'}
            return json.dumps(result, ensure_ascii=False)  # 将字典转换为json串, json是字符串
        else:
            result = {'code': -1, 'message': '账号密码错误'}
            return json.dumps(result, ensure_ascii=False)
    else:
        result = {'code': 10001, 'message': '参数不能为空！'}
        return json.dumps(result, ensure_ascii=False)


# 访问最新测试报告
@server.route('/new_report', methods=['GET', 'POST'])
def api_report():
    try:
        test_report = PROJECT_PATH + r'\api_auto\templates\test_report'
        lists = os.listdir(test_report)  # 返回指定文件夹包含的文件夹或文件名列表
        lists.sort(key=lambda fn: os.path.getmtime(test_report + "/" + fn))  # 按时间排序
        file_new = os.path.join(test_report, lists[-1])
        return send_file(file_new)  # 返回服务端静态志远
    except Exception as e:
        resu = {'code': 10002, 'message': '访问测试报告失败{}'.format(e)}
        return json.dumps(resu, ensure_ascii=False)


# 上传文件
@server.route("/tools", methods=['GET', 'POST'])
def tools():
    try:
        if request.method == 'POST':
            f = request.files["file"]
            upload_path = path.join(PROJECT_PATH + r'\api_auto\upload_files')
            catalogue_file = os.listdir(upload_path)[0]  # 获取目录文件名
            os.remove(PROJECT_PATH + r'\api_auto\upload_files\{}'.format(catalogue_file))  # 删除文件
            now = time.strftime("%Y-%m-%d %H-%M-%S")
            file_name = upload_path + '{}.xls'.format(now)
            f.save(file_name)
            result = {'code': 1, 'message': '文件上传成功'}
            return json.dumps(result, ensure_ascii=False)
        return render_template('upload_start.html')
    except Exception as e:
        result = {'code': 10001, 'message': '上传文件失败{}'.format(e)}
        return json.dumps(result, ensure_ascii=False)


# 下载文件
@server.route("/download", methods=['GET', 'POST'])
def download():
    file_path = PROJECT_PATH + r'\api_auto\download_file'
    filename = "API接口模板.xls"
    return send_from_directory(file_path, filename, as_attachment=True)


# 开始执行
@server.route("/start", methods=['GET', 'POST'])
def start():
    try:
        StartFile.run_ddt()
        return render_template('upload_end.html')
    except Exception as e:
        result = {'code': 10001, 'message': '执行失败\n{}'.format(e)}
        return json.dumps(result, ensure_ascii=False)


# 启动定时任务
@server.route('/task', methods=['get', 'post'])
def task():
    try:
        # 获取通过url请求传参的数据
        task_time = request.values.get('time')
        if isinstance(task_time, str):
            Task.timer_start(time)
            result = '定时以启动每{}秒执行一次！'.format(task_time)
            return json.dumps(result, ensure_ascii=False)
        else:
            result = {'code': 10001, 'message': ''}
            return json.dumps(result, ensure_ascii=False)
    except Exception as e:
        result = {'code': 10001, 'message': '启动定时任务失败:{}'.format(e)}
        return json.dumps(result, ensure_ascii=False)


# 查看指定时间报告
@server.route('/report', methods=['GET', 'POST'])
def report():
    path_report = request.values.get('time')
    if path_report != '':
        report_file = 'test_report/{}Auto_api.html'.format(path_report)
        return render_template(report_file)
    else:
        result = {'code': 10001, 'message': '参数不能为空！'}
        return json.dumps(result, ensure_ascii=False)


server.debug = True
server.run(threaded=True, port=3579, host='0.0.0.0')  # 指定端口、host,0.0.0.0代表不管几个网卡，任何ip都可以访问
