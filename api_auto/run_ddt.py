# coding:utf-8
import unittest
from ddt import ddt, data, unpack
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
import time
from api_auto.handle_excel import HandleExcel
from api_auto.requests_api import RunMain
import os
from api_auto.send_wx import RobotMsg
import json
import jsonpath

# from api_auto.send_mail import Send

TRUE = 0  # TRUE = 执行成功次数
FALSE = 0  # FALSE = 执行失败次数
TOTAL = 0  # TOTAL = 执行总次数
START_TIME = datetime.now()  # START_TIME = 开始时间
REPORT_TIME = time.strftime("%Y-%m-%d_%H-%M-%S")  # NOW = 生成报告时间
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__) + os.path.sep + "..")  # PROJECT_PATH = 上上级目录


@ddt
class MyTesting(unittest.TestCase, RunMain, RobotMsg):
    """ ddt数据驱动获取数据报告封装

    1 数据驱动生成HTML报告封装
    2 返回异常发送通知
    3 执行完成发送通知

    """

    @data(*HandleExcel.read_excel())
    @unpack
    def test(self, apiname, url, method, params, headers, correlation, assertassert):
        """

        :param apiname:接口名
        :param url:url
        :param method:请求类型
        :param params:请求参数
        :param headers:请求头
        :param correlation:断言索引
        :param assertassert:断言索引
        """

        global TOTAL
        TOTAL += 1

        try:
            if params != '':
                params = eval(params)  # str转换为dict
            if correlation != '':
                case = correlation.split(",")[0]
                value = correlation.split(",")[1]
                get_json = HandleExcel.correlation1(case)     # 获取关联接口返回内容
                params[value] = str(jsonpath.jsonpath(get_json, '$..{key_name}'.format(value))[0])
        except Exception as e:
            print(str(e))

        response = self.run_main(url, method, params, json.loads(headers))
        re = response.json()  # 获取返回对象实体返回dict
        response_status = response.status_code  # 状态码
        response_code = re['respData']
        response_time = response.elapsed.total_seconds()  # 响应时间单位秒

        response_data_str = ('''{}
            Request
            Headers:{}
            URL:{}
            Params:{}
            {}
            Response
            Status:{} 响应时长:{}
            Json:{}
            {}'''.format('-' * 110, headers, url, params, '-' * 110, response_status, response_time, re, '-' * 110))
        print(response_data_str)

        # 状态码不等于200业务状态码不等于1发送异常通知
        if response_status == 200 and response_code == '1':
            global TRUE
            TRUE += 1
        else:
            RobotMsg.send_false_msg(apiname, response_status, response_code, url, re, REPORT_TIME)

        # 断言
        if assertassert != '':
            for expect in assertassert.split(","):
                self.assertIn(expect, str(re), "断言失败")
        else:
            print('未输入断言内容')

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(MyTesting))

    #   生成测试报告
    with open(PROJECT_PATH + r'/api_auto/templates/test_report/{}Auto_api.html'.format(REPORT_TIME), 'wb') as fp:
        list_file = os.listdir(PROJECT_PATH + r'/api_auto/templates/test_report/')[0]
        os.remove(PROJECT_PATH + r'/api_auto/templates/test_report/' + list_file)  # 删除旧报告文件
        runner = HTMLTestRunner(stream=fp, title=u'接口测试报告', description=u'测试组')
        runner.run(suite)

    # 测试完成发送通知
    end_time = datetime.now()
    time_consuming = (end_time - START_TIME).total_seconds()  # 总耗时
    RobotMsg.send_done_msg(true=TRUE, false=TOTAL - TRUE, total=TOTAL, total_time=time_consuming, now=REPORT_TIME)
    # Send.send_mail(PROJECT_PATH + r'\api_auto\test_report\{}Auto_api.html'.format(REPORT_TIME))
