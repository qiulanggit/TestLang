# coding:utf-8
import xlrd
import xlwt
from xlutils.copy import copy
import os
from api_auto.requests_api import RunMain

''' 封装获取excel操作方法 '''

# # PROJECT_PATH = 上上级目录
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__) + os.path.sep + "..")


# 获取目录下最近的文件
def new_report(test_report=PROJECT_PATH + r'\api_auto\upload_files'):
    lists = os.listdir(test_report)  # 列出目录的下所有文件和文件夹保存到lists
    lists.sort(key=lambda fn: os.path.getmtime(test_report + "\\" + fn))  # 按时间排序
    file_new = os.path.join(test_report, lists[-1])  # 获取最新的文件保存到file_new
    return file_new


class HandleExcel(object):
    def __init__(self):
        self.file = new_report()
        self.sheet_id = 0
        self.data = self.get_data()
        # 为了创建一个实例时就获得Excel的sheet对象，可以在构造器中调用get_data()
        # 因为类在实例化时就会自动调用构造器，这样创建一个实例时就会自动获得sheet对象了

    # 获取一个sheet对象
    def get_data(self):
        data = xlrd.open_workbook(self.file)  # excel路径
        sheet = data.sheet_by_index(self.sheet_id)  # 通过索引获取表格信息
        return sheet

    # 获取excel行数
    def get_rows(self):
        rows = self.data.nrows
        return rows

    # 获取某个单元格
    def get_value(self, row, col):
        value = self.data.cell_value(row, col)
        return value

    # 封装excel的列表常量
    @classmethod
    def get_caseseq(cls):
        ''' return: 获取caseSeq '''
        caseSeq = 0
        return caseSeq

    @classmethod
    def get_apiName(cls):
        apiName = 1
        return apiName

    @classmethod
    def get_header(cls):
        header = 2
        return header

    @classmethod
    def get_url(cls):
        url = 3
        return url

    @classmethod
    def get_params(cls):
        params = 4
        return params

    @classmethod
    def get_mothod(cls):
        mothod = 5
        return mothod

    @classmethod
    def get_correlation(cls):
        correlation = 6
        return correlation

    @classmethod
    def get_run(cls):
        run = 7
        return run

    @classmethod
    def get_assert(cls):
        assertassert = 8
        return assertassert

    @classmethod
    def get_expectvalue(cls):
        expectvalue = 9
        return expectvalue

    # 向某个单元格写入数据
    def write_excel(self, row, col, value, file_path=new_report()):
        data = xlrd.open_workbook(file_path)  # 打开文件
        data_copy = copy(data)  # 复制源文件
        sheet = data_copy.get_sheet(0)  # 取得复制文件的sheet对象
        sheet.write(row, col, value)  # 某个单元格写入value
        data_copy.save(file_path)  # 保存文件

    # 行
    @classmethod
    def read_row(cls, row, file_path=new_report()):
        workbook = xlrd.open_workbook(file_path)
        sheet = workbook.sheet_by_index(0)
        sum_row = workbook.nrows  # 获取总行数
        if sum_row > row:
            exce_row = sheet.row_values(row)
            return exce_row
        else:
            print('未查找到该行数据')

    # 列
    @classmethod
    def read_col(cls, col, file_path=new_report()):
        workbook = xlrd.open_workbook(file_path)
        sheet = workbook.sheet_by_index(0)
        exce_row = sheet.col_values(col)
        return exce_row

    # 单元格
    @classmethod
    def read_cell(cls, row, col, file_path=new_report()):
        workbook = xlrd.open_workbook(file_path)
        sheet = workbook.sheet_by_index(0)
        excel_cell = sheet.cell(row, col)
        return excel_cell

    # 返回请求参数
    @classmethod
    def read_excel(cls):
        """

        :return:String
        """
        cls.data = HandleExcel()
        rows_count = cls.data.get_rows()  # 获取excel行数
        excel_list = []
        for i in range(1, rows_count):
            url = cls.data.get_value(i, cls.data.get_url())
            params = cls.data.get_value(i, cls.data.get_params())  # 请求体
            run = cls.data.get_value(i, cls.data.get_run())  # 是否执行
            method = cls.data.get_value(i, cls.data.get_mothod())  # 请求类型
            apiname = cls.data.get_value(i, cls.data.get_apiName())  # 接口名
            headers = cls.data.get_value(i, cls.data.get_header())  # 请求头
            correlation = cls.data.get_value(i, cls.data.get_correlation())  # 关联data
            assertassert = cls.data.get_value(i, cls.data.get_assert())  # 断言
            if run == 'YES':
                excel_list.append((apiname, url, method, params, headers, correlation, assertassert))
            else:
                continue
        return excel_list

    # 一级关联
    @classmethod
    def correlation1(cls, correlation_case):
        cls.data = HandleExcel()
        row = int(correlation_case)
        url = cls.data.get_value(row, cls.data.get_url())
        params = cls.data.get_value(row, cls.data.get_params())
        method = cls.data.get_value(row, cls.data.get_mothod())
        headers = cls.data.get_value(row, cls.data.get_header())
        corre = cls.data.get_value(row, cls.data.get_correlation())
        if corre != '':
            params.update(HandleExcel.correlation2(corre))
        r = RunMain.run_main(url, method, eval(params), headers)
        re = r.json()
        return re

    # 二级关联
    @classmethod
    def correlation2(cls, correlation):
        cls.data = HandleExcel()
        row1 = correlation.split(",")[0]
        field = correlation.split(",")[1]
        row = int(row1)
        url = cls.data.get_value(row, cls.data.get_url())
        params = cls.data.get_value(row, cls.data.get_params())
        method = cls.data.get_value(row, cls.data.get_mothod())
        headers = cls.data.get_value(row, cls.data.get_header())
        corre = cls.data.get_value(row, cls.data.get_correlation())
        if corre != '':
            params.update(HandleExcel.correlation3(corre))
        r = RunMain.run_main(url, method, eval(params), headers)
        re = r.json()
        corre = re['data'][field]
        return {field: corre}

    # 三级关联
    @classmethod
    def correlation3(cls, correlation):
        cls.data = HandleExcel()
        row1 = correlation.split(",")[0]
        field = correlation.split(",")[1]
        row = int(row1)
        url = cls.data.get_value(row, cls.data.get_url())
        params = cls.data.get_value(row, cls.data.get_params())
        method = cls.data.get_value(row, cls.data.get_mothod())
        headers = cls.data.get_value(row, cls.data.get_header())
        corre = cls.data.get_value(row, cls.data.get_correlation())
        if corre != '':
            params.update(HandleExcel.correlation1(corre))
        r = RunMain.run_main(url, method, eval(params), headers)
        re = r.json()
        corre = re['data'][field]
        return {field: corre}


if __name__ == '__main__':
    test = HandleExcel()

    print(test.read_excel())
