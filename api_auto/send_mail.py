# coding=utf-8
from email.mime.text import MIMEText  # 专门发送正文
from email.mime.multipart import MIMEMultipart  # 发送多个部分
from email.mime.application import MIMEApplication  # 发送附件
import smtplib  # 发送邮件

''' 发送邮件封装 '''


class Send(object):
    @classmethod
    def send_mail(cls, file_path, theme='API接口测试报告', receive_users='812747769@qq.com'):
        send_user = 'qiulangmail@qq.com'  # 发件人
        password = 'vlqgvdoezkvpbcia'  # 授权码/密码

        subject = theme  # 邮件主题
        email_text = '== API接口测试请查收 =='  # 邮件正文
        server_address = 'imap.qq.com'  # 服务器地址
        mail_type = '1'  # 邮件类型

        # 构造一个邮件体：正文 附件
        msg = MIMEMultipart()
        msg['Subject'] = subject  # 主题
        msg['From'] = send_user  # 发件人
        msg['To'] = receive_users  # 收件人可为list

        # 构建正文
        part_text = MIMEText(email_text)
        msg.attach(part_text)  # 把正文加到邮件体里面去

        # 构建邮件附件
        # file = file           #获取文件路径
        part_attach1 = MIMEApplication(open(file_path, 'rb').read())  # 打开附件
        part_attach1.add_header('Content-Disposition', 'attachment', filename=file_path)  # 为附件命名
        msg.attach(part_attach1)  # 添加附件

        # 发送邮件 SMTP
        smtp = smtplib.SMTP(server_address, 25)  # 连接服务器，SMTP_SSL是安全传输

        smtp.login(send_user, password)
        smtp.sendmail(send_user, receive_users, msg.as_string())  # 发送邮件


if __name__ == '__main__':
    T = Send()
    file_path = 'D:\\2020-04-01 20-26-15 Auto_api.html'
    theme = 'API测试报告'
    T.send_mail(file_path, theme)
