# coding:utf-8

import unittest, time
import requests
import urllib3

urllib3.disable_warnings()
import json

''' 接口请求封装 '''


class RunMain(object):
    @classmethod
    def send_get(cls, url, params, headers=None):
        res = requests.get(url=url, params=params, headers=headers, timeout=2, verify=False)
        return res

    @classmethod
    def send_post(cls, url, params, headers=None):
        res = requests.post(url=url, params=params, headers=headers, timeout=2, verify=False)
        return res

    @classmethod
    def run_main(cls, url, method, params=None, headers=None):
        if method == 'POST':
            res = cls.send_post(url, params, headers)
        else:
            res = cls.send_get(url, params, headers)
        return res


if __name__ == '__main__':
    url = 'https://app.zhuanzhuan.com/zzopen/zljmall/categoryInfo'
    header = {"Content-Type": "application/json;charset=utf-8",
              "Cookie": "zlj_device_id=93ED53BD-5D98-448C-BDCF-E12091C5A0F4;zlj_network_type=WiFi;t=79;zlj_city_code=1440300;zlj_user_type=1;zlj_system_version=14.4;v=8.4.10;zlj_area_code=1440304;zlj_group_id=10;zlj_device_name=iPhone12,5;"
              }
    # params = {"username": 13760159524, "password": 123456}
    method = 'GET'
    re = RunMain.run_main(url=url, headers=header, method=method)
    print("")
    print(re.json())

