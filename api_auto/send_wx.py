# coding:utf-8
import json
import requests
import os

''' 企业微信机器人发送消息封装 '''

# PROJECT_PATH = 上上级目录
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__) + os.path.sep + "..")
# URL = 机器人的链接
URL = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=0b4b9449-554b-4f61-a6d4-f2af89d266790"


class RobotMsg(object):
    @classmethod
    def send_false_msg(cls, apiname, status, code, url, send_message, now):
        data = json.dumps({
            "msgtype": "markdown",
            "markdown": {
                "content": "**{}接口异常**\n "
                           ">URL: <font color=\"comment\">{}</font> \n"
                           ">状态码: <font color=\"warning\">{}</font> \n"
                           ">code: <font color=\"warning\">{}</font> \n"
                           ">响应内容: <font color=\"comment\">{}</font> \n"
                           ">执行时间: <font color=\"comment\">{}</font> \n"
                           "[点击查看详情](http://localhost:3579/report?time={})"
                    .format(apiname, url, status, code, send_message, now, now)
            }
        })
        requests.post(URL, data, auth=('Content-Type', 'application/json'))

    @classmethod
    def send_done_msg(cls, true, false, total, total_time, now):
        data = json.dumps({
            "msgtype": "news",
            "news": {
                "articles": [{
                    "title": '本轮执行结果',
                    "description": '成功:{}     失败:{} \n总执次数:{}     \n总耗时:{}s\n'
                        .format(true, false, total, total_time),
                    "url": 'http://localhost:3579/report?time={}'.format(now),
                    "picurl": ("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&"
                               "sec=1606481012878&di=32b585429f370e28b1dac74dd47d5b91&imgtype=0&sr"
                               "c=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F19%2F03%2F"
                               "27%2Fe72fc48c12ab303d36600d0d0ec0d01b.jpg")
                }]
            }
        })
        requests.post(URL, data, auth=('Content-Type', 'application/json'))


if __name__ == '__main__':
    pass
