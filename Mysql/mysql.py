# coding:utf-8
import pymysql


class Mysql():
    @classmethod
    def sql(self, sql=None):
        host = ''
        user = ''
        password=''
        database=''
        conn = pymysql.connect(host=host, user=user, password=password, database=database, use_unicode=True,charset='utf8',
                               cursorclass=pymysql.cursors.DictCursor)  # 返回字典类型
        cursor = conn.cursor()  # 获取游标
        cursor.execute(sql)
        b = cursor.fetchall()  # 返回多条记录
        for dr in b:
            print(dr)
        cursor.close()  # 关闭光标
        conn.commit()  # 提交数据库
        conn.close()  # 关闭数据库连接
        # conn.rollback()  #回滚数据


if __name__ == '__main__':
    sql = "update panda.odi_order set total_amount='0.01' where order_no='M200514331087661'"
    Mysql.sql(sql=sql)
