# -*- coding: UTF-8 -*-
import unittest
from selenium import webdriver

from time import sleep
from selenium.webdriver.common.action_chains import ActionChains
from webpassword import password


class Auto_Test(unittest.TestCase, password):
    '''测试用例'''

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get('http://precms.i-weiying.com/#/login')

        self.driver.find_element_by_class_name('el-input__inner').send_keys('qiulang')
        self.driver.find_element_by_xpath('//*[@id="app"]/section/form/div[2]/div/div/input'
                                          ).send_keys('{}'.format(self.get()))
        self.driver.find_element_by_xpath('//*[@id="app"]/section/form/div[3]/div').click()
        sleep(5)
        self.assertIsNotNone(self.driver.find_element_by_class_name('el-submenu__title'), 'ok')

    def tearDown(self):
        self.driver.quit()

    def test_001_logout(self):
        mouse = self.driver.find_element_by_class_name('userinfo-inner')
        ActionChains(self.driver).move_to_element(mouse).perform()
        sleep(3)
        self.driver.find_elements_by_class_name('el-dropdown-menu__item')[0].click()
        sleep(3)
        self.driver.find_element_by_xpath('/test_report/body/div[2]/div/div[3]/button[2]/span').click()
        sleep(3)
        self.assertIsNotNone(self.driver.find_element_by_xpath('//*[@id="app"]/section/form/h3'), u'断言失败')

    def test_002_password(self):
        mouse = self.driver.find_element_by_class_name('userinfo-inner')
        ActionChains(self.driver).move_to_element(mouse).perform()
        sleep(2)
        self.driver.find_elements_by_class_name('el-dropdown-menu__item')[1].click()
        sleep(2)
        self.driver.find_elements_by_class_name('el-input__inner')[8].send_keys('{}'.format(self.get()))
        sleep(2)
        self.driver.find_elements_by_class_name('el-input__inner')[9].send_keys('{}'.format(self.write()))
        sleep(2)
        self.driver.find_elements_by_class_name('el-input__inner')[10].send_keys('{}'.format(self.get()))
        sleep(2)
        self.driver.get_screenshot_as_file(u'D:\\ivyweb\\设置密码.png')
        self.driver.find_elements_by_class_name('el-button--primary')[3].click()
        print '修改密码成功!'
        print '新密码：', self.get()
        sleep(2)
        self.assertIsNotNone(self.driver.find_element_by_xpath('//*[@id="app"]/section/form/h3'), u'断言失败')

    def test_003_mark(self):
        self.driver.find_elements_by_class_name('el-input__inner')[0].send_keys('8273564')
        sleep(2)
        self.assertEqual(
            self.driver.find_element_by_xpath(
                '//*[@id="app"]/section/div[1]/div[2]/section/div/div[2]/section/'
                'div[2]/div[3]/table/tbody/tr[1]/td[3]/div').text, '8273564')

    def test_004_mark(self):
        self.driver.find_elements_by_class_name('el-input__inner')[0].send_keys('9999')
        sleep(2)
        self.assertIsNone(self.driver.find_elements_by_class_name('video_rq')[0], u'断言失败：视频内容存在')

    def test_005_mark(self):
        self.driver.find_elements_by_class_name('el-input__inner')[0].send_keys('abcdefg')
        sleep(2)
        self.assertIsNone(self.driver.find_elements_by_class_name('video_rq')[0], u'断言失败：视频内容存在')


if __name__ == '__main__':
    unittest.main(verbosity=2)
